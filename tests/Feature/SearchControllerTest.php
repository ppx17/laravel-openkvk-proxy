<?php


namespace Tests\Feature;


use Illuminate\Http\Client\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class SearchControllerTest extends TestCase
{
    protected $defaultHeaders = ['accept' => 'application/json'];

    public function test_successful_search()
    {
        Cache::flush();

        $this->fakeQueryAndGet();

        $this
            ->post('/api/v1/search', ['query' => 'test'])
            ->assertSuccessful()
            ->assertSee('Van der Lei techniek B.V.')
            ->assertSee('NL852760103B01');

        Http::assertSent(function (Request $request) {
            return $request->hasHeader('ovio-api-key', 'token')
                && $request->url() === 'https://api.overheid.io/openkvk?query=test';
        });

        Http::assertSent(function (Request $request) {
            return $request->url() === 'https://api.overheid.io/openkvk/hoofdvestiging-58488340-0000-downsized';
        });
    }

    public function test_cache()
    {
        Cache::flush();

        $this->fakeQueryAndGet();

        Collection::times(5)->each(function () {
            $this
                ->post('/api/v1/search', ['query' => 'test'])
                ->assertSuccessful();
        });

        Http::assertSentCount(2);
    }

    public function test_non_alphanumeric_search()
    {
        $this
            ->post('/api/v1/search', ['query' => '%$%#%#'])
            ->assertJsonValidationErrors('query');
    }

    public function test_too_short_query()
    {
        $this
            ->post('/api/v1/search', ['query' => 'ab'])
            ->assertJsonValidationErrors('query');
    }

    public function test_too_long_query()
    {
        $this
            ->post('/api/v1/search', ['query' => str_repeat('a', 256)])
            ->assertJsonValidationErrors('query');
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->app['config']->set('openkvk.token', 'token');
    }

    private function file(string $filename): string
    {
        $result = file_get_contents(base_path('tests/files/' . $filename));
        return $result === false ? '' : $result;
    }

    private function fakeQueryAndGet(): void
    {
        Http::fakeSequence()
            ->pushResponse(Http::response($this->file('openkvk/valid-query-response.json')))
            ->pushResponse(Http::response($this->file('openkvk/valid-get-response.json')));
    }
}
