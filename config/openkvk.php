<?php
return [
    'baseurl' => env('OPENKVK_BASE_URL', 'https://api.overheid.io/'),
    'token' => env('OPENKVK_TOKEN', '')
];
