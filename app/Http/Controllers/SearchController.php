<?php


namespace App\Http\Controllers;


use App\Http\Requests\SearchRequest;
use App\Http\Resources\SearchResult;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Pool;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use JetBrains\PhpStorm\Pure;

class SearchController extends Controller
{
    private const CACHE_TTL_HOURS = 24;
    private string $baseUrl;
    private string $token;

    public function __construct()
    {
        $this->baseUrl = config('openkvk.baseurl');
        $this->token = config('openkvk.token');
    }

    public function __invoke(SearchRequest $request)
    {
        $response = $this->get('openkvk', ['query' => $request->get('query')]);

        $allHrefs = $this->responseToHrefs($response);

        $responses = collect(Http::pool(function (Pool $pool) use ($allHrefs) {
            $this->notInCache($allHrefs)
                ->each(fn(string $href) => $pool
                    ->as($href)
                    ->baseUrl($this->baseUrl)
                    ->withHeaders($this->headers())
                    ->get($href));
        }))
            ->each(function (Response $resp, string $href) {
                Cache::put($this->cacheKey($href), $resp->body());
            })
            ->map(fn(Response $resp) => $resp->body())
            ->concat($this->loadFromCache($allHrefs))
            ->map(fn(string $json) => json_decode($json, true));

        return SearchResult::collection($responses);
    }

    private function getHttpClient(): PendingRequest
    {
        return Http::baseUrl($this->baseUrl)
            ->withHeaders($this->headers());
    }

    private function get(string $href, array $query = []): array
    {
        $data = Cache::remember(
            $this->cacheKey($href, $query),
            now()->addHours(self::CACHE_TTL_HOURS),
            function () use ($href, $query) {
                $response = $this->getHttpClient()->get($href, $query);

                if ($response->successful()) {
                    return $response->body();
                }

                return '{}';
            });

        return json_decode($data, true);
    }

    #[Pure] private function cacheKey(string $href, array $query = []): string
    {
        return 'openapi-get-' . $href . http_build_query($query);
    }

    private function headers(): array
    {
        return ['ovio-api-key' => $this->token, 'accept' => 'application/json'];
    }

    private function responseToHrefs(array $response): Collection
    {
        return collect(Arr::get($response, '_embedded.bedrijf', []))
            ->map(fn(array $company) => Arr::get($company, '_links.self.href'))
            ->filter(fn(null|string $href) => is_string($href));
    }

    private function loadFromCache(Collection $allHrefs): Collection
    {
        return $allHrefs
            ->filter(fn($href) => Cache::has($this->cacheKey($href)))
            ->map(fn($href) => Cache::get($this->cacheKey($href)));
    }

    private function notInCache(Collection $allHrefs): Collection
    {
        return $allHrefs
            ->reject(fn($href) => Cache::has($this->cacheKey($href)));
    }
}
