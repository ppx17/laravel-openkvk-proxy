<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class SearchResult extends JsonResource
{
    public function toArray($request)
    {
        return [
            'companyname' => Arr::get($this->resource, 'handelsnaam', ''),
            'taxnumber' => Arr::get($this->resource, 'BTW', ''),
            'city' => Arr::get($this->resource, 'plaats', ''),
        ];
    }
}
